import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

import view_maker from './modules/view_maker'

export default new Vuex.Store({
  state: {
    theme: 'dark',
  },
  getters: {
    getTheme: state => state.theme,
  },
  actions: {
    setTheme({commit}, name) {
      commit('SET_THEME', name);
    },
  },
  mutations: {
    SET_THEME(state, name) {
      state.theme = name
    },
  },
  modules: {
    view_maker
  }
})

import {copyStructureDefaultState} from "@/functions/common";
import {ActiveCanvasTreeElemPayload} from "@/models/payload/ActiveCanvasTreeElemPayload";

const state = {
  slideIndex: 0,
  initSlidesData: {
    canvasSlide: null,
    canvasSlideFile: null,
    canvasSlideFileBase64: null,
    activeCanvasTreeElement: null,
    canvasTreeElements: [],
  },
  slides: [],
  colorPickerIsOpen: false
};

const getters = {
  /* LOCAL GETTERS */
  getSlideIndex: state => state.slideIndex,
  getCanvasSlides: state => state.slides,
  getCanvasSlide: state => state.slides[state.slideIndex].canvasSlide,
  getCanvasSlideFile: state => state.slides[state.slideIndex].canvasSlideFile,
  getActiveCanvasTreeElem: state => state.slides[state.slideIndex].activeCanvasTreeElement,
  getCanvasTreeElements: state => state.slides[state.slideIndex].canvasTreeElements,
  getColorPickerIsOpen: state => state.colorPickerIsOpen,
};

const actions = {
  loadInitData({commit, dispatch}) {
    dispatch('setCanvasSlides', [state.initSlidesData]); // TODO массив со всеми данными придёт с сервера
  },
  changeSlideIndex({commit}, index) {
    commit('CHANGE_SLIDE_INDEX', index);
  },
  setCanvasSlides({commit}, slides) {
    commit('SET_CANVAS_SLIDES', slides);
  },
  addCanvasSlide({commit, dispatch}) {
    commit('CHANGE_SLIDE_INDEX', state.slideIndex+1);
    commit('ADD_CANVAS_SLIDE');
    commit('SET_CANVAS_SLIDE_FILE', null);
    dispatch('setCanvasTreeElements', [])
  },
  selectCanvasSlide({commit}, index) {
    if (index !== state.slideIndex) {
      commit('CHANGE_SLIDE_INDEX', index);
    }
  },
  setCanvasSlide({commit}, canvas) {
    commit('SET_CANVAS_SLIDE', canvas);
  },
  addCanvasTreeElem({commit}, element) {
    commit('ADD_CANVAS_TREE_ELEM', element);
    commit('SET_ACTIVE_CANVAS_TREE_ELEM',
      new ActiveCanvasTreeElemPayload(element, false));
  },
  clearActiveCanvasTreeElem({commit}) {
    commit('CLEAR_ACTIVE_CANVAS_TREE_ELEM');
  },
  /* LOCAL ACTIONS */
  setActiveCanvasTreeElem({commit}, payload) {
    commit('SET_ACTIVE_CANVAS_TREE_ELEM', payload);
  },
  changeActiveCanvasTreeElem({commit}, payload) { /* Поменять поля объекта */
    commit('CHANGE_ACTIVE_CANVAS_TREE_ELEM', payload);
  },
  setCanvasTreeElements({commit}, elements) {
    commit('SET_CANVAS_TREE_ELEMENTS', elements);
  },
  setCanvasSlideFile({commit}, file) {
    commit('SET_CANVAS_SLIDE_FILE', file);
  },
  setColorPickerIsOpen({commit}, status) {
    commit('SET_COLOR_PICKER_IS_OPEN', status);
  },
};

const mutations = {
  ADD_CANVAS_TREE_ELEM(state, element) {
    state.slides[state.slideIndex].canvasTreeElements
      .forEach(elem => {
        elem.isActive = false;
      });
    element.isActive = true;
    state.slides[state.slideIndex].canvasTreeElements.push(element);
  },
  CLEAR_ACTIVE_CANVAS_TREE_ELEM(state) {
    state.slides[state.slideIndex].canvasTreeElements
      .forEach(elem => {
        elem.isActive = false;
      });
    state.slides[state.slideIndex].activeCanvasTreeElement = null;
  },
  SET_ACTIVE_CANVAS_TREE_ELEM(state, payload) {
    if (payload && payload.elem) {
      const element = payload.elem;
      if (payload.isFigure) {
        const foundElem = state.slides[state.slideIndex].canvasTreeElements
          .find(_elem => _elem.id === element.id);
        if (foundElem) {
          state.slides[state.slideIndex].activeCanvasTreeElement = foundElem;
          state.slides[state.slideIndex].canvasTreeElements.forEach(_elem => {
            _elem.isActive = foundElem.figure.id === _elem.id;
          });
        }
      } else {
        state.slides[state.slideIndex].activeCanvasTreeElement = element;
        state.slides[state.slideIndex].canvasTreeElements.forEach(_elem => {
          _elem.isActive = element.figure.id === _elem.id;
        });
      }
    }
  },
  CHANGE_ACTIVE_CANVAS_TREE_ELEM(state, payload) {
    // state.slides[state.slideIndex].activeCanvasTreeElement = payload;
  },
  /* LOCAL MUTATIONS */
  SET_CANVAS_SLIDES(state, slides) {
    state.slides = slides
  },
  CHANGE_SLIDE_INDEX(state, index) {
    state.slideIndex = index;
  },
  ADD_CANVAS_SLIDE(state) {
    state.slides.push(copyStructureDefaultState(state.initSlidesData));
  },
  SET_CANVAS_SLIDE(state, canvas) {
    state.slides[state.slideIndex].canvasSlide = canvas
  },
  SET_CANVAS_SLIDE_FILE(state, file) {
    if (file) {
      state.slides[state.slideIndex].canvasSlideFile = file;
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => {
        state.slides[state.slideIndex].canvasSlideFileBase64 = reader.result;
      };
    } else {
      state.slides[state.slideIndex].canvasSlideFile = null;
      state.slides[state.slideIndex].canvasSlideFileBase64 = null;
    }
  },
  SET_CANVAS_TREE_ELEMENTS(state, elements) {
    state.slides[state.slideIndex].canvasTreeElements = elements
  },
  SET_COLOR_PICKER_IS_OPEN(state, status) {
    state.colorPickerIsOpen = status;
  },
};

export default {
  state,
  getters,
  actions,
  mutations
}
export class ChangeCanvasTreeElemPayload {
  elem = null;
  valueType = 'width'; /* width | height | top | left */
  value = null;

  constructor(
    elem,
    valueType,
    value,
  ) {
    this.elem = elem;
    this.valueType = valueType;
    this.value = value;
  }
}
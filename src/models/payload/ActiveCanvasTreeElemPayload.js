export class ActiveCanvasTreeElemPayload {
  elem = null;
  isFigure = null;

  constructor(
    elem,
    isFigure,
  ) {
    this.elem = elem;
    this.isFigure = isFigure;
  }
}
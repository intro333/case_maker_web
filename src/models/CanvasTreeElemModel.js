export class CanvasTreeElemModel {
    id = 0;
    level = 0;
    title = '';
    type = 'folder'; /* folder | text | rectangle | circle | arrow */
    figure = null; // fabric object
    isOpen = false;
    isActive = false;
    children = [];

    constructor(
        id,
        level,
        title,
        type,
        figure
    ) {
        this.id = id;
        this.level = level;
        this.title = title;
        this.type = type;
        this.figure = figure;
    }

    setOtherFields(isOpen, isActive, children) {
        this.isOpen = isOpen;
        this.isActive = isActive;
        this.children = children;
    }
}